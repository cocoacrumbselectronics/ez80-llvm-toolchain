ez80-llvm-toolchain
===

This small repo provides a simple bash shell script to setup a complete ez80 toolchain based on LLVM and GNU binutils.

The LLVM/Clang toolchain with the z80/ez80 backend can be found here: https://github.com/jacobly0/llvm-project.
The original GAS patch, necessary for the Clang compiler to emit GNU as (GAS) compatible assembly code can be found here: https://github.com/codebje/ez80-toolchain. It has been slightly adapted so that it can be applied on the Clang-14 code base.

The script simply does the following:

1) Clone the LLVM git repo into llvm-project
2) Apply the GAS patch to llvm-project
3) Compile and install the LLVM tools in $(pwd)/toolchain/ez80-none-elf
4) Download binutils 2.37.
5) Compile and install binutils 2.37 in $(pwd)/toolchain/ez80-none-elf
6) Perform a smoke test to see if we can compile and generate ez80 code correctly.

The full build can take considerable time. On a 12c/24t AMD 3900X, the build took slightly less than 12 minutes.

The smoke test tries to compile this simple C program:

```C
int main(void)
{
	int	a;
	int	b;
	int	c;

	a = 1;
	b = 2;
	c = a + b;

	return c;
} /* end main */
```

When all is done, you should see this output from the smoke test:

```
SMOKE TEST
==========
Compiling a minimal main.c program without optimisation:
Dumping disassembly of 'main-noopt.o':

main-noopt.o:     file format elf32-z80


Disassembly of section .text:

00000000 <_main>:
   0:	dd e5             	push ix
   2:	dd 21 00 00 00    	ld ix,0x0000
   7:	dd 39             	add ix,sp
   9:	21 f4 ff ff       	ld hl,0xfffff4
   d:	39                	add hl,sp
   e:	f9                	ld sp,hl
   f:	b7                	or a,a
  10:	ed 62             	sbc hl,hl
  12:	11 01 00 00       	ld de,0x0001
  16:	01 02 00 00       	ld bc,0x0002
  1a:	dd 2f fd          	ld (ix-3),hl
  1d:	dd 1f fa          	ld (ix-6),de
  20:	dd 0f f7          	ld (ix-9),bc
  23:	dd 27 fa          	ld hl,(ix-6)
  26:	dd 17 f7          	ld de,(ix-9)
  29:	19                	add hl,de
  2a:	dd 2f f4          	ld (ix-12),hl
  2d:	dd 27 f4          	ld hl,(ix-12)
  30:	fd 21 0c 00 00    	ld iy,0x000c
  35:	fd 39             	add iy,sp
  37:	fd f9             	ld sp,iy
  39:	dd e1             	pop ix
  3b:	c9                	ret


====================================

Compiling a minimal main.c program with -O1 optimisation:
Dumping disassembly of 'main-O1opt.o':

main-O1opt.o:     file format elf32-z80


Disassembly of section .text:

00000000 <_main>:
   0:	21 03 00 00       	ld hl,0x0003
   4:	c9                	ret

~/Documents/Projects/eZ80-experiment/setupToolchain

Elapsed time: 0 days 00 hr 11 min 32 sec

====================================
```

