#!/usr/bin/bash

start_time=$(date +%s)

INSTALLDIR=$(pwd)/toolchain/ez80-none-elf
echo $INSTALLDIR

# Cloning the jacobly0/llvm-project git repo
pushd .
git clone -b z80 --depth 1 https://github.com/jacobly0/llvm-project.git
cd llvm-project/
# Apply patch to emit GNU 'as' compatible assembly code
patch -p1 < ../0001-Emit-GAS-syntax-Clang-14.patch
# patch -p1 < ../0002-Fix-lea-zero-offset.patch
patch -p1 < ../0003-Fix-AsciiDirective.patch
# patch -p1 < ../0004-Fix-EI-RETI_partial-fix.patch
patch -p1 < ../0005-Fix-escape-double-quotes.patch
# Create and move to the build directory
mkdir build
cd build
# Create make files
cmake ../llvm -GNinja -DLLVM_ENABLE_PROJECTS="clang" \
    -DCMAKE_INSTALL_PREFIX=$INSTALLDIR \
    -DCMAKE_BUILD_TYPE=Release \
    -DLLVM_EXPERIMENTAL_TARGETS_TO_BUILD=Z80 \
    -DLLVM_TARGETS_TO_BUILD= \
    -DLLVM_DEFAULT_TARGET_TRIPLE=ez80-none-elf
# Build and install Clang 14 with ez80 support
ninja install
popd

# Downloading binutils
pushd .
curl -LO https://ftp.gnu.org/gnu/binutils/binutils-2.37.tar.gz
tar xf binutils-2.37.tar.gz
cd binutils-2.37 \
    && ./configure --target=z80-none-elf --program-prefix=ez80-none-elf- --prefix=$INSTALLDIR \
    && make -j4 \
    && make install
popd

# Creating symbolic links so that we can differentiate this Clang toolchain from the system Clang toolchain
pushd .
cd $PWD/toolchain/ez80-none-elf/bin
ln -s clang   ez80-none-elf-clang
ln -s clang++ ez80-none-elf-clang++
ln -s llc     ez80-none-elf-llc
ln -s opt     ez80-none-elf-opt
popd

# Smoke test
cp main.c $INSTALLDIR/bin/
pushd .
cd $INSTALLDIR/bin
echo ""
echo "SMOKE TEST"
echo "=========="
echo "Compiling a minimal main.c program without optimisation:"
./ez80-none-elf-clang -target ez80-none-elf -mllvm -z80-print-zero-offset -Wa,-march=ez80, -nostdinc main.c -c -o main-noopt.o
echo "Dumping disassembly of 'main-noopt.o':"
./ez80-none-elf-objdump -d main-noopt.o
echo ""
echo ""
echo "===================================="
echo ""
echo "Compiling a minimal main.c program with -O1 optimisation:"
./ez80-none-elf-clang -target ez80-none-elf -mllvm -z80-print-zero-offset -Wa,-march=ez80 -nostdinc main.c -c -O1 -o main-O1opt.o
echo "Dumping disassembly of 'main-O1opt.o':"
./ez80-none-elf-objdump -d main-O1opt.o
echo ""
popd

end_time=$(date +%s)
elapsed=$(( end_time - start_time ))

echo ""
eval "echo Elapsed time: $(date -ud "@$elapsed" +'$((%s/3600/24)) days %H hr %M min %S sec')"
echo ""
echo "===================================="
echo ""
echo "You can find the complete toolchain here:" $INSTALLDIR
echo "If you want you can delete the 'llvm-project' repo, the 'binutils-2.37'"
echo "folder and the binutils-2.37.tar.gz file to save some space."
echo ""
